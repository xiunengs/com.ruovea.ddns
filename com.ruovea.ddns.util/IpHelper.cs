﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace com.ruovea.ddns.util
{
    public class IpHelper
    {
        public static string CurrentIp()
        {
            try
            {
                // https://2021.ipchaxun.com/
                var res = HttpHelper.GetAsync("https://2021.ip138.com/").Result;

                var mc = Regex.Match(res, @"\d+.\d+.\d+.\d+");
                if (mc.Success)
                {
                    return mc.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex) {
                return string.Empty;
            }
        }

    }
}
