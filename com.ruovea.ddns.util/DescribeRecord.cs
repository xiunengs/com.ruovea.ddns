﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.ruovea.ddns.util
{
    public class DescribeRecord
    {
        public string DomainName { get; set; }
        public string RecordId { get; set; }
        public string RR { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public long? TTL { get; set; }
        public long? Priority { get; set; }
        public string Line { get; set; }
        public string Status { get; set; }
        public bool? Locked { get; set; }
        public int? Weight { get; set; }
    }
}
