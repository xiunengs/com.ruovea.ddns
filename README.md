# DDNS

#### 介绍
DDNS（Dynamic Domain Name Server，动态域名服务）是将用户的动态IP地址映射到一个固定的域名解析服务上，用户每次连接网络的时候客户端程序就会通过信息传递把该主机的动态IP地址传送给位于服务商主机上的服务器程序，服务器程序负责提供DNS服务并实现动态域名解析。


# com.ruovea.ddns
自建ddns服务，将域名映射到内网计算机，可通过域名访问内网计算机。
定时查询域名解析的A记录，宽带的ip改变后自动更新A记录的ip值


# 百度的解释
DDNS（Dynamic Domain Name Server，动态域名服务）是将用户的动态IP地址映射到一个固定的域名解析服务上，用户每次连接网络的时候客户端程序就会通过信息传递把该主机的动态IP地址传送给位于服务商主机上的服务器程序，服务器程序负责提供DNS服务并实现动态域名解析。


# 使用net 5.0 编写 跨平台 简单易用 


## 快速开始
1.修改配置文件`appsettings.json`
```
    "ddnsName": "aliyun",

  "aliyun": {
    //
    "accessKey": "accessKey",
    // accessSecret
    "accessSecret": "accessSecret",
    // 顶级域名
    "domain": "test.com",
    // 不进行更新的RR值，|分割，等值匹配
    "ignoreRR": "*.sg|sg"
  },
  // 腾讯dnspod
  "dnspod": {
    "token": "token",
    "recordId": null,
    "domain": null,

    "email": "",
    "password": ""
  }
```
2.运行程序

## 支持的域名解析服务商
- [x] 阿里云
- [x] 腾讯dnspod

- [ ] ...

## 原理介绍
定时查询内网电脑所有的宽带的公网ip，通过开放接口查询域名解析记录解析的ip地址，对比两个值，如果不同，通过接口更新域名解析记录的值。

## 支持
![微信](https://images.gitee.com/uploads/images/2021/0611/181118_373a8607_958369.jpeg "微信图片_20210611180033.jpg")

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0611/181139_b082cf51_958369.jpeg "微信图片_20210611180040.jpg")
