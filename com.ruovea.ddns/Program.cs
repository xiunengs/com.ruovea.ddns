using com.ruovea.aliyun.ddns.api;
using com.ruovea.ddns.util;
using com.ruovea.tencent.dnspod;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace com.ruovea.ddns
{
    public class Program
    {
        static void Main(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).Build();

            //ddns名称
            string ddnsName = configuration.GetValue("ddnsName", "aliyun");
            // 心跳记录
            int secondsTime = configuration.GetValue("secondsTime", 60 * 1000);

            IDomainRecord domainRecord;
            switch (ddnsName)
            {
                case "aliyun":
                        domainRecord = new AliyunDomainRecord( configuration);
                    break;
                case "dnspod":
                    domainRecord = new TencentDomainRecord(configuration);
                    break;
                default:
                    Console.WriteLine($"不支持的开放平台 {ddnsName}");
                    return;
            }

            while (true)
            {
                try
                {
                    domainRecord.Refresh();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Thread.Sleep(secondsTime);  //心跳
            }
        }
    }
}
